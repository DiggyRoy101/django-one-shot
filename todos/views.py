from django.shortcuts import get_object_or_404, redirect, render
from .models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm

# Create your views here.


def todo_list_list(request):
    list = TodoList.objects.all()
    context = {
        "todolist": list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    task = get_object_or_404(TodoList, id=id)
    context = {
        "task_object": task,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", id=task.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=post)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", id=task.id)
    else:
        form = TodoListForm(instance=post)

        context = {
            "form": form,
        }
        return render(request, "todos/update.html", context)


def todo_list_delete(request, id):
    post = TodoList.objects.get(id=id)
    if request.method == "POST":
        post.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", task.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/itemcreate.html", context)


def todo_item_update(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=post)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", task.list.id)
    else:
        form = TodoItemForm(instance=post)

        context = {
            "form": form,
        }
        return render(request, "todos/itemupdate.html", context)
